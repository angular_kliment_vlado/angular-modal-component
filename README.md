# `angular-modal-component` � 

## Getting Started

To get you started you can simply clone the `angular-modal-component` repository and install the dependencies:

### Prerequisites

You need git to clone the `angular-modal-component` repository. You can get git from [here][git].

We also use a number of Node.js tools to initialize and test `angular-modal-component`. You must have Node.js
and its package manager (npm) installed. You can get them from [here][node].

### Clone `angular-modal-component`

Clone the `angular-modal-component` repository using git:

```
git clone git@bitbucket.org:angular_kliment_vlado/angular-modal-component.git
cd angular-seed
```

### Install Dependencies

We have preconfigured `npm` to automatically run `bower` so we can simply do:

```
npm install
```

Behind the scenes this will also call `bower install`. After that, you should find out that you have
two new folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `app/bower_components` - contains the Angular framework files

*Note that the `bower_components` folder would normally be installed in the root folder but
`angular-seed` changes this location through the `.bowerrc` file. Putting it in the `app` folder
makes it easier to serve the files by a web server.*

### Run the Application

We have preconfigured the project with a simple development web server. The simplest way to start
this server is:

```
npm start
```

Now browse to the app at [`localhost:8000/index.html`][local-app-url].


## Directory Layout

```
app/                    --> all of the source files for the application
  app.css               --> default stylesheet
  components/           --> all app specific modules
    modal/              --> Directory for modal Component
     modalComponent.js --> Component file responsible for the logic inside the modal time tracking
     modal_component.html --> The layout with the view logic
     modal_component.css  --> Styles for modal component
  directives/            --> In here we put all directives
    open_modal/          --> Responsible for opening the modal and passing params from the parent controller
      open_modal.html    --> Shape the button view
      openModal.js       --> Holding, remodeling the data coming from the controller and passing it to the component
  view1/                --> the view1 view template and logic
    view1.html            --> the partial template
    view1.js              --> the controller logic
    view1_test.js         --> tests of the controller
  view2/                --> the view2 view template and logic
    view2.html            --> the partial template
    view2.js              --> the controller logic
    view2_test.js         --> tests of the controller
  app.js                --> main application module
  index.html            --> app layout file (the main html template file of the app)
  index-async.html      --> just like index.html, but loads js files asynchronously
karma.conf.js         --> config file for running unit tests with Karma
e2e-tests/            --> end-to-end tests
  protractor-conf.js    --> Protractor config file
  scenarios.js          --> end-to-end scenarios to be run by Protractor
```

[angularjs]: https://angularjs.org/
[bower]: http://bower.io/
[git]: https://git-scm.com/
[http-server]: https://github.com/indexzero/http-server
[jasmine]: https://jasmine.github.io/
[jdk]: https://wikipedia.org/wiki/Java_Development_Kit
[jdk-download]: http://www.oracle.com/technetwork/java/javase/downloads
[karma]: https://karma-runner.github.io/
[local-app-url]: http://localhost:8000/index.html
[node]: https://nodejs.org/
[npm]: https://www.npmjs.org/
[protractor]: http://www.protractortest.org/
[selenium]: http://docs.seleniumhq.org/
[travis]: https://travis-ci.org/
[travis-docs]: https://docs.travis-ci.com/user/getting-started
