'use strict';

/**
 *
 * @returns {{
	 * 	restrict: string,
	  * templateUrl: string,
	  * scope: {
	  * 	name: string,
	  * 	data: object},
	  * link: link}
  * }
 */
function openModal($uibModal){
	return{
		restrict: 'AEC',
		templateUrl: './directives/open_modal/open_modal.html',
		scope: {

		},

		link:function(scope, element, attr){
			var $ctrl = this;
			var modalParams = {};
			if(attr.openModal){
				modalParams = scope.$eval(attr.openModal);
			}

			element.on('click', function(){
				scope.open(); // Real opening of modal
			});

			$ctrl.close = function(){
				console.log('closed');
			};

			$ctrl.ok = function () {

				$uibModalInstance.close();
			};

			$ctrl.cancel = function () {
				console.log('kensel');
				$uibModalInstance.dismiss('cancel');
			};
			/**
			 *
			 */
			scope.open = function () {
				var modalInstance = $uibModal.open({
					animation: true,
					component: 'modalComponent',
					resolve: {
						data: function () {
							return modalParams;
						}
					}
				});

				modalInstance.result.then(function (selectedItem) {
					scope.selected = selectedItem;
				}, function () {
					console.log(scope.selected);
					console.log('modal-component dismissed at: ' + new Date());
				});
			}
		}
	};
}

angular
	.module('myApp.directives.open_modal', ['ui.bootstrap'])
	.directive('openModal', openModal);