angular.module('myApp')
	.factory('notificationInterceptor', ['$q', '$log',  function($q, $log) {
	$log.debug('$log is here to show you that this is a regular factory with injection');

	var notificationInterceptor = {
		response: function(response) {
			response.config.responseTimestamp = new Date().getTime();
			return response;
		}
	};

	return notificationInterceptor;
}])
	.config(['$httpProvider', function($httpProvider) {
		$httpProvider.interceptors.push('notificationInterceptor');
	}]);