/**
 *
 * @constructor
 */
function ResourceService($http){
	/**
	 * This is an object which hold the form data from @modalComponent
	 * @type {{id: number, trackable_id: null, trackable_type: string, description: string, time_start: Date, duration: number}}
	 */
	this.emptyResourceModel = function(){
		return {
			id: 0,
			trackable_id: null,
			trackable_type: {
				id: 0,
				name: ''
			},
			description: '',
			time_start: new Date(),
			duration: 0
		};
	};
	/**
	 * @param resource
	 * @returns {*}
	 */
	this.getName = function(resource){
		return resource['name'];
	};
	/**
	 * @param resource
	 * @returns {{resources: [*,*]}}
	 */
	this.getResourceTypes = function(resource){
		var resName = this.getName(resource);
		var fromDB = [
			{
				name: 'Finance',
				types: [
					'one',
					'two',
					'three'
				]
			},
			{
				name: 'CRM',
				types: [
					'four',
					'five',
					'six'
				]
			}
		];
		/**
		 * Return only the Array witch match name with the given name of resource
		 */
		function findResourceTypes(resource){
			return resource.name === resName;
		}

		return fromDB.find(findResourceTypes);
	};
	/**
	 *
	 * @param data
	 */
	this.setResourceData = function(data){
		return $http({
			url: '/api/v1/success',
			method: 'POST',
			data: data,
			apiMock: true

		});
	};
}
angular.module('myApp.services.resource',[])
	.service('ResourceService', ResourceService);