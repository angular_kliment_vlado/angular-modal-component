angular.module('myApp.welcome.welcomeComponent')
	.component('welcomeComponent',{
		templateUrl: '/welcome_component.html',
		bindings: {
			name: "="
		},
		controller: function(){
			var name = this.name;
			console.log(name);
		}
	});