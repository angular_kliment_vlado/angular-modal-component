angular.module('myApp.directives.open_modal').component('modalComponent', {
	templateUrl: './components/modal/modal_component.html',
	bindings: {
		resolve: '<',
		close: '&',
		dismiss: '&'
	},
	controller: ['ResourceService','Notification', '$http', function (ResourceService, Notification, $http) {
		var $ctrl = this;
		$ctrl.resTypes = []; // hold the resource types
		/**
		 * Init function. Prepare all data Here
		 */
		$ctrl.$onInit = function () {
			$ctrl.data 			= $ctrl.resolve.data;
			$ctrl.resourceModel = ResourceService.emptyResourceModel();
			$ctrl.resourceModel.id = $ctrl.data.selectedType.id;
			$ctrl.datetime 		= new Date();
			$ctrl.duration  	= [5, 10, 15, 30, 45, 60, 90, 120];
			$ctrl.chosenDuration= 0;
			$ctrl.calendarOpened = false;
			$ctrl.dateOptions = {
				formatYear: 'yy',
				maxDate: new Date(2020, 5, 22),
				minDate: new Date(),
				startingDay: 1
			};

			this.modalOptions = {
				delay: 5000,
				startTop: 20,
				startRight: 10,
				verticalSpacing: 20,
				horizontalSpacing: 20,
				positionX: 'center',
				positionY: 'top'
			};
		};
		/**
		 *
		 * @param changes
		 */
		$ctrl.$onChanges = function (changes) {
			console.log(changes);
		};
		/**
		 * @return array of string types
		 * @param type
		 */
		$ctrl.getResourceTypes = function(type){
			$ctrl.resourceModel 			   = ResourceService.emptyResourceModel();
			$ctrl.resourceModel.trackable_type = ResourceService.getResourceTypes(type);
			$ctrl.resourceModel.id			   = type.id;

			return $ctrl.resourceModel.trackable_type;
			// ResourceService.getResourceTypes(type)
			// 	.then(function(data){
			// 		console.log(data);
			// 	}).resolve(function(error){
			// 	console.log(error);
			// });
		};

		$ctrl.ok = function () {
			ResourceService.setResourceData($ctrl.resourceModel)
				.then(function(res){
					console.log(res);
					$ctrl.createNotification(res.data.response.message);
				})
				.catch(function(rej){
					$ctrl.createNotification(rej.data.response.message);
				});

			$ctrl.close({$value: $ctrl.data});
		};

		$ctrl.cancel = function () {
			console.log('hey');
			$ctrl.dismiss({$value: 'cancel'});
		};

		$ctrl.changeDateTime = function () {
			$ctrl.resourceModel.datetime = $ctrl.datetime;
		};

		$ctrl.openCalendar = function(){
			$ctrl.calendarOpened = true;
		};
		/**
		 *  modalOptions - global inherited options
		 * @param message - to show in the notification
		 */
		$ctrl.createNotification = function(message){
			this.modalOptions['message'] = message;
			Notification.success(this.modalOptions);
		};
	}]
});