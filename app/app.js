'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
	'ui-notification',
	'ui.bootstrap',
	'ngRoute',
	'myApp.view1',
	'myApp.view2',
	'myApp.directives.open_modal',
	'myApp.version',
	'myApp.services.resource',
	'apiMock'

]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/view1'});
}]);

angular.module('myApp.directives.open_modal', ['ui-notification'])
	.config(function(NotificationProvider) {
		NotificationProvider.setOptions({
			delay: 10000,
			startTop: 20,
			startRight: 10,
			verticalSpacing: 20,
			horizontalSpacing: 20,
			positionX: 'left',
			positionY: 'bottom'
		});
	});
