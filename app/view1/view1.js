'use strict';

function ViewCtrl($scope, $uibModal) {
    var $ctrl = this;
    var size = null;
    var parentSelector = null;

	$scope.data = {
	    modalParams: {
	      size: size,
            parentSelector: parentSelector
        },
		logType: [
			{id: 0, name: '---Make a choice---'},
			{id: 1, name: 'Finance'},
			{id: 2, name: 'CRM'}
		],
		selectedType: {id: 0, name: '---Make a choice---'}
	};

	$scope.ok = function(){

    };
	$ctrl.ok = function(){

	};
}


angular.module('myApp.view1', ['ngRoute'])
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider.when('/view1', {
			templateUrl: 'view1/view1.html',
			controller: 'View1Ctrl'
		});

	}])
	.controller('View1Ctrl', ViewCtrl);